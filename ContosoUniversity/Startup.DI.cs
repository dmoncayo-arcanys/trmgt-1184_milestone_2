﻿using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Repositories.Repository;
using ContosoUniversity.Services.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.DependencyInjection;

namespace ContosoUniversity
{
    public partial class Startup
    {
        private static void ConfigureDI(IServiceCollection services)
        {
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IInstructorService, InstructorService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IDepartmentService, DepartmentService>();

            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<IInstructorRepository, InstructorRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
        }
    }
}
