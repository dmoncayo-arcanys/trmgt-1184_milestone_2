﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Pages.Courses
{
    public class EditModel : DepartmentNamePageModel
    {
        private readonly ICourseService courseService;
        private readonly IDepartmentService departmentService;

        [BindProperty]
        public Course Course { get; set; }

        public EditModel(ICourseService courseService, IDepartmentService departmentService)
        {
            this.courseService = courseService;
            this.departmentService = departmentService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Course = courseService.Get(id);

            if (Course == null)
            {
                return NotFound();
            }

            PopulateDepartmentsDropDownList(departmentService);
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!courseService.Update(Course))
            {
                return NotFound();
            }

            return RedirectToPage("./Index");
        }
    }
}
