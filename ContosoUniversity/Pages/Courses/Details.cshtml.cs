﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Courses
{
    public class DetailsModel : PageModel
    {
        private readonly ICourseService courseService;

        public Course Course { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="courseService"></param>
        public DetailsModel(ICourseService courseService)
        {
            this.courseService = courseService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Course = courseService.Get(id);

            if (Course == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
