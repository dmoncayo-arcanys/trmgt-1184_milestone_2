﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Courses
{
    public class DeleteModel : PageModel
    {
        private readonly ICourseService courseService;

        [BindProperty]
        public Course Course { get; set; }

        public string ErrorMessage { get; set; }

        public DeleteModel(ICourseService courseService)
        {
            this.courseService = courseService;
        }

        public IActionResult OnGet(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Course = courseService.Get(id);

            if (Course == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_003;
            }

            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (!courseService.Delete(id))
            {
                return RedirectToAction("./Delete", new { id, saveChangesError = true });
            }

            return RedirectToPage("./Index");
        }
    }
}
