﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Pages.Courses
{
    public class CreateModel : DepartmentNamePageModel
    {
        private readonly ICourseService courseService;
        private readonly IDepartmentService departmentService;

        [BindProperty]
        public Course Course { get; set; }

        public CreateModel(ICourseService courseService, IDepartmentService departmentService)
        {
            this.courseService = courseService;
            this.departmentService = departmentService;
        }

        public IActionResult OnGet()
        {
            PopulateDepartmentsDropDownList(departmentService);
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            courseService.Insert(Course);

            return RedirectToPage("./Index");
        }
    }
}
