﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Pages.Courses
{
    public class IndexModel : PageModel
    {
        private readonly ICourseService courseService;

        public PaginatedList<Course> Course { get; set; }

        public string NameSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="courseService"></param>
        public IndexModel(ICourseService courseService)
        {
            this.courseService = courseService;
        }

        public void OnGet(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Course> courseIQ = courseService.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                courseIQ = courseIQ.Where(s => s.Title.Contains(searchString));
            }

            courseIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => courseIQ.OrderByDescending(s => s.Title),
                _ => courseIQ.OrderBy(s => s.Title),
            };

            Course = PaginatedList<Course>.Create(courseIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
