﻿using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace ContosoUniversity.Pages.Courses
{
    public class DepartmentNamePageModel : PageModel
    {
        public SelectList DepartmentNameSL { get; set; }

        public void PopulateDepartmentsDropDownList(IDepartmentService departmentService, object selectedDepartment = null)
        {
            var departmentsQuery = departmentService.GetAll().OrderBy(i => i.Name);
            DepartmentNameSL = new SelectList(departmentsQuery, "DepartmentID", "Name", selectedDepartment);
        }
    }
}
