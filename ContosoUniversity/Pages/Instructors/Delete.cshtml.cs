﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Instructors
{
    public class DeleteModel : PageModel
    {
        private readonly IInstructorService instructorService;

        [BindProperty]
        public Instructor Instructor { get; set; }

        public string ErrorMessage { get; set; }

        public DeleteModel(IInstructorService instructorService)
        {
            this.instructorService = instructorService;
        }

        public IActionResult OnGet(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Instructor = instructorService.Get(id);

            if (Instructor == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_002;
            }

            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (!instructorService.Delete(id))
            {
                return RedirectToAction("./Delete", new { id, saveChangesError = true });
            }

            return RedirectToPage("./Index");
        }
    }
}
