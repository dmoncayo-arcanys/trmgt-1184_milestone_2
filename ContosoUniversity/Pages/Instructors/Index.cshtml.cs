﻿using ContosoUniversity.Models;
using ContosoUniversity.Models.ViewModels;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;

namespace ContosoUniversity.Pages.Instructors
{
    public class IndexModel : PageModel
    {
        private readonly IInstructorService instructorService;

        public InstructorIndexData Instructor { get; set; }

        public int InstructorID { get; set; }

        public int CourseID { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instructorService"></param>
        public IndexModel(IInstructorService instructorService)
        {
            this.instructorService = instructorService;
        }

        public void OnGet(int? id, int? courseID)
        {
            Instructor = new InstructorIndexData();
            Instructor.Instructors = instructorService.GetAll();
            if (id != null)
            {
                InstructorID = id.Value;
                Instructor instructor = Instructor.Instructors.Single(i => i.ID == id.Value);
                Instructor.Courses = instructor.CourseAssignments.Select(s => s.Course);
            }

            if (courseID != null)
            {
                CourseID = courseID.Value;
                Instructor.Enrollments = Instructor.Courses.Single(x => x.CourseID == courseID).Enrollments;
            }
        }
    }
}
