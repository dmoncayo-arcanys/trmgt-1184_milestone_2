﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Instructors
{
    public class DetailsModel : PageModel
    {
        private readonly IInstructorService instructorService;

        public Instructor Instructor { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instructorService"></param>
        public DetailsModel(IInstructorService instructorService)
        {
            this.instructorService = instructorService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Instructor = instructorService.Get(id);

            if (Instructor == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
