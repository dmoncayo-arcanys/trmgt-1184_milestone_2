﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Pages.Instructors
{
    public class EditModel : InstructorCoursesPageModel
    {
        private readonly IInstructorService instructorService;
        private readonly ICourseService courseService;

        [BindProperty]
        public Instructor Instructor { get; set; }

        public EditModel(IInstructorService instructorService, ICourseService courseService)
        {
            this.instructorService = instructorService;
            this.courseService = courseService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Instructor = instructorService.Get(id);

            if (Instructor == null)
            {
                return NotFound();
            }

            PopulateAssignedCourseData(courseService, Instructor);
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPost(string[] selectedCourses)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Instructor results = instructorService.Get(Instructor.ID);

            if (results == null)
            {
                return NotFound();
            }

            Instructor.CourseAssignments = results.CourseAssignments;

            UpdateInstructorCourses(courseService, selectedCourses, Instructor);

            if (!instructorService.Update(Instructor))
            {
                return NotFound();
            }

            return RedirectToPage("./Index");
        }
    }
}
