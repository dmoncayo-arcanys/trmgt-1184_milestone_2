﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Instructors
{
    public class CreateModel : PageModel
    {
        private readonly IInstructorService instructorService;

        [BindProperty]
        public Instructor Instructor { get; set; }

        public CreateModel(IInstructorService instructorService)
        {
            this.instructorService = instructorService;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            instructorService.Insert(Instructor);

            return RedirectToPage("./Index");
        }
    }
}
