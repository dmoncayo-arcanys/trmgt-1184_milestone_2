﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Pages.Departments
{
    public class EditModel : InstructorNamePageModel
    {
        private readonly IDepartmentService departmentService;
        private readonly IInstructorService instructorService;

        [BindProperty]
        public Department Department { get; set; }

        public EditModel(IDepartmentService departmentService, IInstructorService instructorService)
        {
            this.departmentService = departmentService;
            this.instructorService = instructorService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Department = departmentService.Get(id);

            if (Department == null)
            {
                return NotFound();
            }

            PopulateIntructorsDropDownList(instructorService);
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!departmentService.Update(Department))
            {
                return NotFound();
            }

            return RedirectToPage("./Index");
        }
    }
}
