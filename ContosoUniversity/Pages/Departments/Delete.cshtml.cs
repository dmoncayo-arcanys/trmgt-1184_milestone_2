﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Departments
{
    public class DeleteModel : PageModel
    {
        private readonly IDepartmentService departmentService;

        [BindProperty]
        public Department Department { get; set; }

        public string ErrorMessage { get; set; }

        public DeleteModel(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        public IActionResult OnGet(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Department = departmentService.Get(id);

            if (Department == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_004;
            }

            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (!departmentService.Delete(id))
            {
                return RedirectToAction("./Delete", new { id, saveChangesError = true });
            }

            return RedirectToPage("./Index");
        }
    }
}
