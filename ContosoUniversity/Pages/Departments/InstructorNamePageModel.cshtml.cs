﻿using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace ContosoUniversity.Pages.Departments
{
    public class InstructorNamePageModel : PageModel
    {
        public SelectList InstructorNameSL { get; set; }

        public void PopulateIntructorsDropDownList(IInstructorService instructorService, object selectedInstructor = null)
        {
            var departmentsQuery = instructorService.GetAll().OrderBy(i => i.FullName);
            InstructorNameSL = new SelectList(departmentsQuery, "ID", "FullName", selectedInstructor);
        }
    }
}
