﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Pages.Departments
{
    public class CreateModel : InstructorNamePageModel
    {
        private readonly IDepartmentService departmentService;
        private readonly IInstructorService instructorService;

        [BindProperty]
        public Department Department { get; set; }

        public CreateModel(IDepartmentService departmentService, IInstructorService instructorService)
        {
            this.departmentService = departmentService;
            this.instructorService = instructorService;
        }

        public IActionResult OnGet()
        {
            PopulateIntructorsDropDownList(instructorService);
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            departmentService.Insert(Department);

            return RedirectToPage("./Index");
        }
    }
}
