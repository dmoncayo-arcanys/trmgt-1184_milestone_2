﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Departments
{
    public class DetailsModel : PageModel
    {
        private readonly IDepartmentService departmentService;

        public Department Department { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="departmentService"></param>
        public DetailsModel(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Department = departmentService.Get(id);

            if (Department == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
