﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Pages.Departments
{
    public class IndexModel : PageModel
    {
        private readonly IDepartmentService departmentService;

        public PaginatedList<Department> Department { get; set; }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="departmentService"></param>
        public IndexModel(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        public void OnGet(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;
            DateSort = sortOrder == Constants.FILTER_DATE ? Constants.FILTER_DATE_DESC : Constants.FILTER_DATE;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Department> departmentIQ = departmentService.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                departmentIQ = departmentIQ.Where(s => s.Name.Contains(searchString));
            }

            departmentIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => departmentIQ.OrderByDescending(s => s.Name),
                Constants.FILTER_DATE => departmentIQ.OrderBy(s => s.StartDate),
                Constants.FILTER_DATE_DESC => departmentIQ.OrderByDescending(s => s.StartDate),
                _ => departmentIQ.OrderBy(s => s.Name),
            };

            Department = PaginatedList<Department>.Create(departmentIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
