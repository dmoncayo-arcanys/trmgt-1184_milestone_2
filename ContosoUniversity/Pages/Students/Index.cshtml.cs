﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Pages.Students
{
    public class IndexModel : PageModel
    {
        private readonly IStudentService studentService;

        public PaginatedList<Student> Student { get; set; }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="studentService"></param>
        public IndexModel(IStudentService studentService)
        {
            this.studentService = studentService;
        }

        public void OnGet(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;
            DateSort = sortOrder == Constants.FILTER_DATE ? Constants.FILTER_DATE_DESC : Constants.FILTER_DATE;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Student> studentIQ = studentService.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                studentIQ = studentIQ.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstMidName.Contains(searchString));
            }

            studentIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => studentIQ.OrderByDescending(s => s.LastName),
                Constants.FILTER_DATE => studentIQ.OrderBy(s => s.EnrollmentDate),
                Constants.FILTER_DATE_DESC => studentIQ.OrderByDescending(s => s.EnrollmentDate),
                _ => studentIQ.OrderBy(s => s.LastName),
            };

            Student = PaginatedList<Student>.Create(studentIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
