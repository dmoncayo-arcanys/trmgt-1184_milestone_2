﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Students
{
    public class DetailsModel : PageModel
    {
        private readonly IStudentService studentService;

        public Student Student { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="studentService"></param>
        public DetailsModel(IStudentService studentService)
        {
            this.studentService = studentService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Student = studentService.Get(id);

            if (Student == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
