﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Students
{
    public class DeleteModel : PageModel
    {
        private readonly IStudentService studentService;

        [BindProperty]
        public Student Student { get; set; }

        public string ErrorMessage { get; set; }

        public DeleteModel(IStudentService studentService)
        {
            this.studentService = studentService;
        }

        public IActionResult OnGet(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Student = studentService.Get(id);

            if (Student == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_001;
            }

            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (!studentService.Delete(id))
            {
                return RedirectToAction("./Delete", new { id, saveChangesError = true });
            }

            return RedirectToPage("./Index");
        }
    }
}
