﻿using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages.Students
{
    public class EditModel : PageModel
    {
        private readonly IStudentService studentService;

        [BindProperty]
        public Student Student { get; set; }

        public EditModel(IStudentService studentService)
        {
            this.studentService = studentService;
        }

        public IActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Student = studentService.Get(id);

            if (Student == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!studentService.Update(Student))
            {
                return NotFound();
            }

            return RedirectToPage("./Index");
        }
    }
}
