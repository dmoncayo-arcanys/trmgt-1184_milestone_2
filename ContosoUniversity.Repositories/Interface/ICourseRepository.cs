﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Repositories.Interface
{
    public interface ICourseRepository : IDisposable
    {
        IEnumerable<Course> GetCourses();
        Course GetCourseByID(int id);
        Course GetCourseDetailsByID(int id);
        void InsertCourse(Course course);
        void InsertCourseAssignment(CourseAssignment course);
        void DeleteCourse(int id);
        void DeleteCourseAssignment(CourseAssignment courseToRemove);
        void UpdateCourse(Course course);
        void Save();
    }
}
