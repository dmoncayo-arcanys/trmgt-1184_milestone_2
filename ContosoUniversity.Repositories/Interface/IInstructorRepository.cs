﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Repositories.Interface
{
    public interface IInstructorRepository : IDisposable
    {
        IEnumerable<Instructor> GetInstructors();
        Instructor GetInstructorByID(int id);
        Instructor GetInstructorDetailsByID(int id);
        void InsertInstructor(Instructor instructor);
        void DeleteInstructor(int id);
        void UpdateInstructor(Instructor instructor);
        void Save();
    }
}
