﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Repositories.Interface
{
    public interface IStudentRepository : IDisposable
    {
        IEnumerable<Student> GetStudents();
        Student GetStudentByID(int id);
        Student GetStudentDetailsByID(int id);
        void InsertStudent(Student student);
        void DeleteStudent(int id);
        void UpdateStudent(Student student);
        void Save();
    }
}
