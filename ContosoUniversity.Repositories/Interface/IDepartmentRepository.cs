﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Repositories.Interface
{
    public interface IDepartmentRepository : IDisposable
    {
        IEnumerable<Department> GetDepartments();
        Department GetDepartmentByID(int id);
        Department GetDepartmentDetailsByID(int id);
        void InsertDepartment(Department department);
        void DeleteDepartment(int id);
        void UpdateDepartment(Department department);
        void Save();
    }
}
