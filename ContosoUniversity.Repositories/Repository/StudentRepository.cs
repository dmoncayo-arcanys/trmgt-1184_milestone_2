﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Repositories.Repository
{
    public class StudentRepository : IStudentRepository, IDisposable
    {
        private readonly SchoolContext context;

        public StudentRepository(SchoolContext context)
        {
            this.context = context;
        }

        public IEnumerable<Student> GetStudents()
        {
            return context.Student
                .Include(s => s.Enrollments)
                .ThenInclude(e => e.Course)
                .AsNoTracking()
                .ToList();
        }

        public Student GetStudentByID(int id)
        {
            return context.Student.Find(id);
        }

        public Student GetStudentDetailsByID(int id)
        {
            return context.Student
                .Include(s => s.Enrollments)
                .ThenInclude(e => e.Course)
                .AsNoTracking()
                .FirstOrDefault(m => m.ID == id);
        }

        public void InsertStudent(Student student)
        {
            context.Student.Add(student);
        }

        public void DeleteStudent(int id)
        {
            Student student = context.Student.Find(id);
            context.Student.Remove(student);
        }

        public void UpdateStudent(Student student)
        {
            context.Entry(student).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
