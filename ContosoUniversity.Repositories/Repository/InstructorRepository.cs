﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Repositories.Repository
{
    public class InstructorRepository : IInstructorRepository, IDisposable
    {
        private readonly SchoolContext context;

        public InstructorRepository(SchoolContext context)
        {
            this.context = context;
        }

        public IEnumerable<Instructor> GetInstructors()
        {
            return context.Instructor
                .Include(i => i.OfficeAssignment)
                .Include(i => i.CourseAssignments)
                .ThenInclude(i => i.Course)
                    .ThenInclude(i => i.Department)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                        .ThenInclude(i => i.Enrollments)
                            .ThenInclude(i => i.Student)
                .AsNoTracking()
                .ToList();
        }

        public Instructor GetInstructorByID(int id)
        {
            return context.Instructor.Find(id);
        }

        public Instructor GetInstructorDetailsByID(int id)
        {
            return context.Instructor
                .Include(i => i.OfficeAssignment)
                .Include(i => i.CourseAssignments)
                .ThenInclude(i => i.Course)
                    .ThenInclude(i => i.Department)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                        .ThenInclude(i => i.Enrollments)
                            .ThenInclude(i => i.Student)
                .AsNoTracking()
                .FirstOrDefault(m => m.ID == id);
        }

        public void InsertInstructor(Instructor instructor)
        {
            context.Instructor.Add(instructor);
        }

        public void DeleteInstructor(int id)
        {
            Instructor instructor = context.Instructor.Find(id);
            context.Instructor.Remove(instructor);
        }

        public void UpdateInstructor(Instructor instructor)
        {
            context.Entry(instructor).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
