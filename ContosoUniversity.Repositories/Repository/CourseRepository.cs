﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Repositories.Repository
{
    public class CourseRepository : ICourseRepository, IDisposable
    {
        private readonly SchoolContext context;

        public CourseRepository(SchoolContext context)
        {
            this.context = context;
        }

        public IEnumerable<Course> GetCourses()
        {
            return context.Course
                .Include(s => s.Department)
                .Include(s => s.Department.Administrator)
                .Include(s => s.CourseAssignments)
                .AsNoTracking()
                .ToList();
        }

        public Course GetCourseByID(int id)
        {
            return context.Course.Find(id);
        }

        public Course GetCourseDetailsByID(int id)
        {
            return context.Course
                .Include(s => s.Department)
                .Include(s => s.Department.Administrator)
                .Include(s => s.CourseAssignments)
                .AsNoTracking()
                .FirstOrDefault(m => m.CourseID == id);
        }

        public void InsertCourseAssignment(CourseAssignment course)
        {
            context.CourseAssignment.Add(course);
        }

        public void InsertCourse(Course course)
        {
            context.Course.Add(course);
        }

        public void DeleteCourse(int id)
        {
            Course course = context.Course.Find(id);
            context.Course.Remove(course);
        }

        public void DeleteCourseAssignment(CourseAssignment courseToRemove)
        {
            context.CourseAssignment.Remove(courseToRemove);
        }

        public void UpdateCourse(Course course)
        {
            context.Entry(course).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
