﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Repositories.Repository
{
    public class DepartmentRepository : IDepartmentRepository, IDisposable
    {
        private readonly SchoolContext context;

        public DepartmentRepository(SchoolContext context)
        {
            this.context = context;
        }

        public IEnumerable<Department> GetDepartments()
        {
            return context.Department
                .Include(s => s.Administrator)
                .AsNoTracking()
                .ToList();
        }

        public Department GetDepartmentByID(int id)
        {
            return context.Department.Find(id);
        }

        public Department GetDepartmentDetailsByID(int id)
        {
            return context.Department
                .Include(s => s.Administrator)
                .AsNoTracking()
                .FirstOrDefault(m => m.DepartmentID == id);
        }

        public void InsertDepartment(Department department)
        {
            context.Department.Add(department);
        }

        public void DeleteDepartment(int id)
        {
            Department department = context.Department.Find(id);
            context.Department.Remove(department);
        }

        public void UpdateDepartment(Department department)
        {
            context.Entry(department).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
