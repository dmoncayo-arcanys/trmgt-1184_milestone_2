﻿namespace ContosoUniversity.Commons
{
    public class Messages
    {
        public const string ERROR_001 = "Delete student record failed. Please try again.";
        public const string ERROR_002 = "Delete instructor record failed. Please try again.";
        public const string ERROR_003 = "Delete course record failed. Please try again.";
        public const string ERROR_004 = "Delete department record failed. Please try again.";
    }
}
