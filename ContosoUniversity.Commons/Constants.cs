﻿namespace ContosoUniversity.Commons
{
    public class Constants
    {
        // DB
        public const string DB_CONTEXT = "SchoolContext";

        // Pagination
        public const int PAGE_CONTENT = 5;

        // Filter
        public const string FILTER_NAME_DESC = "name_desc";
        public const string FILTER_DATE = "date";
        public const string FILTER_DATE_DESC = "date_desc";
    }
}
