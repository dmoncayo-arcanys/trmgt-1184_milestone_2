﻿using ContosoUniversity.Models;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Interface
{
    public interface IStudentService
    {
        Student Get(int? id);
        IEnumerable<Student> GetAll();
        bool Update(Student student);
        bool Delete(int? id);
        bool Insert(Student student);
    }
}
