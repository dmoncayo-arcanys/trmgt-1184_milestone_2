﻿using ContosoUniversity.Models;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Interface
{
    public interface IDepartmentService
    {
        Department Get(int? id);
        IEnumerable<Department> GetAll();
        bool Update(Department department);
        bool Delete(int? id);
        bool Insert(Department department);
    }
}
