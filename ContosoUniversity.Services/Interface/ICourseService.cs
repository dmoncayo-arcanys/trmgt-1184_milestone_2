﻿using ContosoUniversity.Models;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Interface
{
    public interface ICourseService
    {
        Course Get(int? id);
        IEnumerable<Course> GetAll();
        bool Update(Course course);
        bool Delete(int? id);
        bool DeleteCourseAssignment(CourseAssignment courseAssignment);
        bool Insert(Course course);
        bool InsertCourseAssignment(CourseAssignment courseAssignment);
    }
}
