﻿using ContosoUniversity.Models;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Interface
{
    public interface IInstructorService
    {
        Instructor Get(int? id);
        IEnumerable<Instructor> GetAll();
        bool Update(Instructor instructor);
        bool Delete(int? id);
        bool Insert(Instructor instructor);
    }
}
