﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Service
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository repository;
        private readonly ILogger<CourseService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public CourseService(ICourseRepository repository, ILogger<CourseService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Get course by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Course</returns>
        public Course Get(int? id)
        {
            Course course = null;
            try
            {
                course = repository.GetCourseDetailsByID((int)id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Get course.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Course> GetAll()
        {
            IEnumerable<Course> course = null;
            try
            {
                course = repository.GetCourses();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Update course record.
        /// </summary>
        /// <param name="course"></param>
        /// <returns>bool</returns>
        public bool Update(Course course)
        {
            bool flag = true;
            try
            {
                repository.UpdateCourse(course);
                repository.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (repository.GetCourseByID(course.CourseID).CourseID > 0)
                {
                    flag = false;
                }
                else
                {
                    logger.LogError(ex.ToString());
                    throw;
                }
            }
            return flag;
        }

        /// <summary>
        /// Delete course record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                repository.DeleteCourse((int)id);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Delete course assignment record.
        /// </summary>
        public bool DeleteCourseAssignment(CourseAssignment courseAssignment)
        {
            bool flag = true;
            try
            {
                repository.DeleteCourseAssignment(courseAssignment);
                //repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool Insert(Course course)
        {
            bool flag = true;
            try
            {
                repository.InsertCourse(course);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }


        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool InsertCourseAssignment(CourseAssignment courseAssignment)
        {
            bool flag = true;
            try
            {
                repository.InsertCourseAssignment(courseAssignment);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
