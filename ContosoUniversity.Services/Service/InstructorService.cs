﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Service
{
    public class InstructorService : IInstructorService
    {
        private readonly IInstructorRepository repository;
        private readonly ILogger<InstructorService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public InstructorService(IInstructorRepository repository, ILogger<InstructorService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Get instructor by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Instructor</returns>
        public Instructor Get(int? id)
        {
            Instructor instructor = null;
            try
            {
                instructor = repository.GetInstructorDetailsByID((int)id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return instructor;
        }

        /// <summary>
        /// Get instructor.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Instructor> GetAll()
        {
            IEnumerable<Instructor> instructor = null;
            try
            {
                instructor = repository.GetInstructors();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return instructor;
        }

        /// <summary>
        /// Update instructor record.
        /// </summary>
        /// <param name="instructor"></param>
        /// <returns>bool</returns>
        public bool Update(Instructor instructor)
        {
            bool flag = true;
            try
            {
                repository.UpdateInstructor(instructor);
                repository.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (repository.GetInstructorByID(instructor.ID).ID > 0)
                {
                    flag = false;
                }
                else
                {
                    logger.LogError(ex.ToString());
                    throw;
                }
            }
            return flag;
        }

        /// <summary>
        /// Delete instructor record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                repository.DeleteInstructor((int)id);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool Insert(Instructor student)
        {
            bool flag = true;
            try
            {
                repository.InsertInstructor(student);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
