﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Service
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository repository;
        private readonly ILogger<StudentService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public StudentService(IStudentRepository repository, ILogger<StudentService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Get student by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Student</returns>
        public Student Get(int? id)
        {
            Student student = null;
            try
            {
                student = repository.GetStudentDetailsByID((int)id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return student;
        }

        /// <summary>
        /// Get students.
        /// </summary>
        /// <returns>IEnumerable<Student></returns>
        public IEnumerable<Student> GetAll()
        {
            IEnumerable<Student> students = null;
            try
            {
                students = repository.GetStudents();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return students;
        }

        /// <summary>
        /// Update student record.
        /// </summary>
        /// <param name="student"></param>
        /// <returns>bool</returns>
        public bool Update(Student student)
        {
            bool flag = true;
            try
            {
                repository.UpdateStudent(student);
                repository.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (repository.GetStudentByID(student.ID).ID > 0)
                {
                    flag = false;
                }
                else
                {
                    logger.LogError(ex.ToString());
                    throw;
                }
            }
            return flag;
        }

        /// <summary>
        /// Delete student record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                repository.DeleteStudent((int)id);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool Insert(Student student)
        {
            bool flag = true;
            try
            {
                repository.InsertStudent(student);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
