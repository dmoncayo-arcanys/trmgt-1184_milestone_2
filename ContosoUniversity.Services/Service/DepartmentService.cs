﻿using ContosoUniversity.Models;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace ContosoUniversity.Services.Service
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository repository;
        private readonly ILogger<DepartmentService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public DepartmentService(IDepartmentRepository repository, ILogger<DepartmentService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Get department by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Course</returns>
        public Department Get(int? id)
        {
            Department department = null;
            try
            {
                department = repository.GetDepartmentDetailsByID((int)id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return department;
        }

        /// <summary>
        /// Get course.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Department> GetAll()
        {
            IEnumerable<Department> department = null;
            try
            {
                department = repository.GetDepartments();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return department;
        }

        /// <summary>
        /// Update department record.
        /// </summary>
        /// <param name="department"></param>
        /// <returns>bool</returns>
        public bool Update(Department department)
        {
            bool flag = true;
            try
            {
                repository.UpdateDepartment(department);
                repository.Save();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (repository.GetDepartmentByID(department.DepartmentID).DepartmentID > 0)
                {
                    flag = false;
                }
                else
                {
                    logger.LogError(ex.ToString());
                    throw;
                }
            }
            return flag;
        }

        /// <summary>
        /// Delete course record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                repository.DeleteDepartment((int)id);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool Insert(Department department)
        {
            bool flag = true;
            try
            {
                repository.InsertDepartment(department);
                repository.Save();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
